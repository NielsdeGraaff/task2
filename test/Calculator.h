
#ifndef TASK2_CALCULATOR_H
#define TASK2_CALCULATOR_H


class Calculator {
public:
    int add(int op1, int op2);
    double add(double op1, double op2);

    int subtract(int op1, int op2);
    double subtract(double op1, double op2);

    int multiply(int op1, int op2);
    double multiply(double op1, double op2);

    int divide(int op1, int op2);
    double divide(double op1, double op2);

    int square(int op);
    double square(double op);

};

#endif //TASK2_CALCULATOR_H
