//
// Created by niels on 20-9-2018.
//

#include <gtest/gtest.h>
#include "Calculator.h"

namespace {
    Calculator calc;

/*
 * Add Tests
 */
    TEST(AddTestInt, PosNeg) {
        EXPECT_EQ(0, calc.add(10, -10));
    }

    TEST(AddTestInt, PosPos) {
        EXPECT_EQ(20, calc.add(10, 10));
    }

    TEST(AddTestInt, PosZero) {
        EXPECT_EQ(10, calc.add(10, 0));
    }

    TEST(AddTestDouble, PosNeg) {
        EXPECT_EQ(0.0, calc.add(10.0, -10.0));
    }

    TEST(AddTestDouble, PosPos) {
        EXPECT_EQ(20.0, calc.add(10.0, 10.0));
    }

    TEST(AddTestDouble, PosZero) {
        EXPECT_EQ(10.0, calc.add(10.0, 0.0));
    }


/*
 * Subtract Tests
 */
    TEST(SubtractTestInt, PosNeg) {
        EXPECT_EQ(15, calc.subtract(10, -5));
    }

    TEST(SubtractTestInt, PosPos) {
        EXPECT_EQ(0, calc.subtract(10, 10));
    }

    TEST(SubtractTestInt, PosZero) {
        EXPECT_EQ(10, calc.subtract(10, 0));
    }

    TEST(SubtractTestDouble, PosNeg) {
        EXPECT_EQ(15.0, calc.subtract(10.0, -5.0));
    }

    TEST(SubtractTestDouble, PosPos) {
        EXPECT_EQ(0.0, calc.subtract(10.0, 10.0));
    }

    TEST(SubtractTestDouble, PosZero) {
        EXPECT_EQ(10.0, calc.subtract(10.0, 0.0));
    }

/*
 * Multiply Tests
 */
    TEST(MultiplyTestInt, PosNeg) {
        EXPECT_EQ(-20, calc.multiply(10, -2));
    }

    TEST(MultiplyTestInt, PosPos) {
        EXPECT_EQ(100, calc.multiply(10, 10));
    }

    TEST(MultiplyTestInt, PosZero) {
        EXPECT_EQ(0, calc.multiply(10, 0));
    }

    TEST(MultiplyTestDouble, PosNeg) {
        EXPECT_EQ(-20.0, calc.multiply(10.0, -2.0));
    }

    TEST(MultiplyTestDouble, PosPos) {
        EXPECT_EQ(100.0, calc.multiply(10.0, 10.0));
    }

    TEST(MultiplyTestDouble, PosZero) {
        EXPECT_EQ(0.0, calc.multiply(10.0, 0.0));
    }

/*
 * Divide tests
 */

    TEST(DivideTestInt, PosNeg) {
        EXPECT_EQ(-5, calc.divide(10, -2));
    }

    TEST(DivideTestInt, PosPos) {
        EXPECT_EQ(5, calc.divide(25, 5));
    }

    TEST(DivideTestInt, PosZero) {
        EXPECT_THROW({
             try {
                 calc.divide(1, 0);
                 }
                     catch (const char* msg) {
                         EXPECT_STREQ("Division by zero condition!", msg);
                         throw;
                         }
                     }, std::domain_error);
    }

    TEST(DivideTestDouble, PosNeg) {
        EXPECT_EQ(-5.0, calc.divide(10.0, -2.0));
}

    TEST(DivideTestDouble, PosPos) {
        EXPECT_EQ(5.0, calc.divide(25.0, 5.0));
    }

    TEST(DivideTestDouble, PosZero) {
        EXPECT_THROW({
                         try {
                             calc.divide(1.0, 0.0);
                         }
                         catch (const char* msg) {
                             EXPECT_STREQ("Division by zero condition!", msg);
                             throw;
                         }
                     }, std::domain_error);
    }

/*
 * Square tests
 */
    TEST(SquareTestInt, Pos) {
        EXPECT_EQ(25, calc.square(5));
    }

    TEST(SquareTestInt, Neg) {
        EXPECT_EQ(25, calc.square(-5));
    }

    TEST(SquareTestInt, Zero) {
        EXPECT_EQ(0, calc.square(0));
    }

    TEST(SquareTestDouble, Pos) {
        EXPECT_EQ(25.0, calc.square(5.0));
    }

    TEST(SquareTestDouble, Neg) {
        EXPECT_EQ(25.0, calc.square(-5.0));
    }

    TEST(SquareTestDouble, Zero) {
        EXPECT_EQ(0.0, calc.square(0.0));
    }
}
