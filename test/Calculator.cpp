
#include <stdexcept>
#include "Calculator.h"

int Calculator::add(int op1, int op2) {
    return op1+op2;
}

double Calculator::add(double op1, double op2) {
    return op1+op2;
}

int Calculator::subtract(int op1, int op2){
    return op1-op2;
}

double Calculator::subtract(double op1, double op2) {
    return op1-op2;
}

int Calculator::multiply(int op1, int op2) {
    return op1*op2;
}

double Calculator::multiply(double op1, double op2) {
    return op1*op2;
}

int Calculator::divide(int op1, int op2) {
    if(op2 == 0)
        throw std::domain_error("Division by zero condition!");
    else
    return op1/op2;
}

double Calculator::divide(double op1, double op2) {
    if(op2 == 0)
        throw std::domain_error("Division by zero condition!");
    else
        return op1/op2;
}

int Calculator::square(int op) {
    return op*op;
}

double Calculator::square(double op) {
    return op*op;
}